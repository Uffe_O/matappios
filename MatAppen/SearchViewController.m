//
//  SearchViewController.m
//  MatAppen
//
//  Created by ITHS on 2016-03-14.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "SearchViewController.h"
#import "FoodTableViewController.h"
#import "MyDataHandler.h"

@interface SearchViewController ()
@property (weak, nonatomic) IBOutlet UITextField *searchText;
@property (nonatomic) NSMutableArray *searchData;
@property (nonatomic) MyDataHandler *handler;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UILabel *loadingText;
@property (weak, nonatomic) IBOutlet UIView *loadingView;

@property (nonatomic) int positionX;
@property (nonatomic) int positionY;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    CGRect labelFrame = CGRectMake(25, 100, 280, 150);
    UILabel *myLabel = [[UILabel alloc] initWithFrame:labelFrame];
    
    [myLabel setBackgroundColor:[UIColor blueColor]];
    [myLabel setTextColor:[UIColor whiteColor]];
    [myLabel setTextAlignment:NSTextAlignmentCenter];
    
    NSString *labelText = @"Här kan du söka på matprodukter, sen får du fram en lista av produkter du kan trycka vidare på som sen visar näringsinnehåll.";
    [myLabel setText:labelText];
    
    myLabel.numberOfLines = 0;
    [myLabel sizeToFit];
    [self.view addSubview:myLabel];
    
    _positionX = self.loadingView.frame.origin.x;
    _positionY = self.loadingView.frame.origin.y;
}

-(void)viewWillAppear:(BOOL)animated{
    self.loadingView.hidden = YES;
    self.loadingText.hidden = YES;
    [UIView animateWithDuration:1.0 delay:0.0 options:kNilOptions animations:^{
        self.loadingView.center = CGPointMake(_positionX, _positionY+7);
    }completion:^(BOOL finished) {
    }];
}

-(void)loopThroughAnimation{
    [UIView animateWithDuration:1.0 delay:0.0 options:kNilOptions animations:^{
        self.loadingView.center = CGPointMake(_positionX+125, _positionY+7);
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 delay:0.0 options:kNilOptions animations:^{
            self.loadingView.center = CGPointMake(_positionX, _positionY+7);
        }completion:^(BOOL finished) {
            [self loopThroughAnimation];
        }];
    }];
}

- (IBAction)readyTheInformation:(UIButton *)sender {
    self.loadingView.hidden = NO;
    self.loadingText.hidden = NO;
    [self loopThroughAnimation];
    self.handler = [[MyDataHandler alloc]init];
    self.searchData = [NSMutableArray array];
    
    [self.handler getArrayWithNameAndNumber:self.searchText.text onDone:^{
        [self allDataReceived];
    }];
}

-(void)allDataReceived {
    [self performSegueWithIdentifier:@"GoToTableView" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    FoodTableViewController *transferItem = segue.destinationViewController;
    
    self.searchData = [self.handler getArrayList];
    
    if([segue.identifier isEqualToString:@"GoToTableView"]){
        transferItem.mySearchText = self.searchText.text;
        transferItem.foodItems = self.searchData;
        NSLog(@"Search count: %d", self.searchData.count);
    }
    else
    {
        assert(NO);
    }
}

@end
