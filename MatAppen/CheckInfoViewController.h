//
//  CheckInfoViewController.h
//  MatAppen
//
//  Created by ITHS on 2016-02-23.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodWare.h"

@interface CheckInfoViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *energiText;
@property (weak, nonatomic) IBOutlet UILabel *proteinText;
@property (weak, nonatomic) IBOutlet UILabel *fettText;
@property (weak, nonatomic) IBOutlet UILabel *vitaminerText;
@property (weak, nonatomic) IBOutlet UILabel *matvaraText;
@property (weak, nonatomic) IBOutlet UILabel *nyttighetText;

@property (nonatomic) FoodWare *activeItem;

@end
