//
//  WelcomeViewController.m
//  MatAppen
//
//  Created by ITHS on 2016-03-10.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController ()

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;
@property (weak, nonatomic) IBOutlet UIButton *searchListButton;
@property (weak, nonatomic) IBOutlet UIButton *appInfoButton;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.iconImage.image = [UIImage imageNamed:@"burger120"];
    self.backgroundImage.image = [UIImage imageNamed:@"Lemon"];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.searchListButton]];
    [self.animator addBehavior:self.gravity];
    
    self.collision = [[UICollisionBehavior alloc]initWithItems:@[self.searchListButton]];
    CGPoint rightEdge = CGPointMake(self.appInfoButton.frame.origin.x + self.appInfoButton.frame.size.width, self.appInfoButton.frame.origin.y);
    [self.collision addBoundaryWithIdentifier:@"appInfoButton" fromPoint:self.appInfoButton.frame.origin toPoint:rightEdge];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
