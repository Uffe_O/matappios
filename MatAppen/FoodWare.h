//
//  FoodWare.h
//  MatAppen
//
//  Created by ITHS on 2016-02-23.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodWare : NSObject

-(instancetype) initWithText: (NSString *)text andEnergi: (NSString*)energi
                  andProtein:(NSString*)protein
                     andFett:(NSString*)fett
                andVitaminer:(NSString*)vitaminer;

@property (nonatomic) NSString *text;
@property (nonatomic) NSString *energi;
@property (nonatomic) NSString *protein;
@property (nonatomic) NSString *fett;
@property (nonatomic) NSString *vitaminer;

@end
