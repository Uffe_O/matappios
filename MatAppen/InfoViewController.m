//
//  InfoViewController.m
//  MatAppen
//
//  Created by ITHS on 2016-03-19.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()

@end

@implementation InfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    CGRect labelFrame = CGRectMake(20, 100, 280, 150);
    UILabel *myLabel = [[UILabel alloc] initWithFrame:labelFrame];
    
    [myLabel setBackgroundColor:[UIColor greenColor]];
    
    NSString *labelText = @"Den här appen skapades för att underlätta för de som vill hitta och sedan veta mer om innehållet i en matprodukt. ";
    [myLabel setText:labelText];
    
    myLabel.numberOfLines = 0;
    [myLabel sizeToFit];
    [self.view addSubview:myLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
