//
//  MyDataHandler.m
//  MatAppen
//
//  Created by ITHS on 2016-03-15.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "MyDataHandler.h"
#import "FoodWare.h"

@implementation MyDataHandler

-(void)getArrayWithNameAndNumber: (NSString *) textWithSearch onDone:(OnDone)onDone {
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@", textWithSearch];
    NSURL *url = [NSURL URLWithString:s];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        self.resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        [self getArrayWithFoodData:onDone];
        
        if (error) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
        }
        
    }];

    [task resume];
}

-(void)getArrayWithFoodData:(OnDone)onDone {
    self.foodItemList = [NSMutableArray array];
    
    for (int i = 0; i < self.resultData.count; i++) {
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@",self.resultData[i][@"number"]];
        NSURL *url = [NSURL URLWithString:s];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if (error) {
                NSLog(@"Error: %@", error);
                return;
            }
            
            NSError *jsonParseError = nil;
            
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
            
            if (error) {
                NSLog(@"Failed to parse data: %@", jsonParseError);
            }
            
            self.protein = result[@"nutrientValues"][@"protein"];
            self.fett = result[@"nutrientValues"][@"fat"];
            self.vitaminC = result[@"nutrientValues"][@"vitaminC"];
            self.energi = result[@"nutrientValues"][@"energyKcal"];
        
        FoodWare *newFoodWare = [[FoodWare alloc]initWithText:[NSString stringWithFormat:@"%@",self.resultData[i][@"name"]] andEnergi:[NSString stringWithFormat:@"%@", self.energi] andProtein:[NSString stringWithFormat:@"%@", self.protein] andFett:[NSString stringWithFormat:@"%@", self.fett] andVitaminer:[NSString stringWithFormat:@"%@", self.vitaminC]];
        
        [self.foodItemList addObject:newFoodWare];
            
            //NSLog(@"Nr of food wares in food item list: %d", self.foodItemList.count);
            if(self.foodItemList.count == self.resultData.count) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    onDone();
                });
            }
            
        }];
        
        [task resume];
        }
}

-(id)getArrayList{
    return self.foodItemList;
}

@end
