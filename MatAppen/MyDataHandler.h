//
//  MyDataHandler.h
//  MatAppen
//
//  Created by ITHS on 2016-03-15.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyDataHandler : NSObject

@property (nonatomic, strong) NSArray *resultData;
@property (nonatomic, strong) NSMutableArray *foodItemList;

@property (nonatomic) NSString *protein;
@property (nonatomic) NSString *fett;
@property (nonatomic) NSString *vitaminC;
@property (nonatomic) NSString *energi;

typedef void (^OnDone)();

-(void)getArrayWithNameAndNumber: (NSString *) textWithSearch onDone:(OnDone)onDone;

-(void)getArrayWithFoodData:(OnDone)onDone;

-(id)getArrayList;

@end
