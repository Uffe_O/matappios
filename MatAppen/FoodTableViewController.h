//
//  FoodTableViewController.h
//  MatAppen
//
//  Created by ITHS on 2016-02-23.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodTableViewController : UITableViewController

@property (nonatomic, strong) NSString *mySearchText;
@property (nonatomic, strong) NSMutableArray *foodItems;

@end
