//
//  CheckInfoViewController.m
//  MatAppen
//
//  Created by ITHS on 2016-02-23.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "CheckInfoViewController.h"

@interface CheckInfoViewController ()

@end

@implementation CheckInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.matvaraText.text = self.activeItem.text;
    self.energiText.text = [NSString stringWithFormat:@"energi(kcal): %@", self.activeItem.energi];
    self.proteinText.text = [NSString stringWithFormat:@"protein: %@", self.activeItem.protein];
    self.fettText.text = [NSString stringWithFormat: @"fett: %@", self.activeItem.fett];
    self.vitaminerText.text = [NSString stringWithFormat: @"VitaminC: %@", self.activeItem.vitaminer];
    
    if (self.activeItem.fett.intValue > 15) {
        self.nyttighetText.text = @"Det är inte nyttigt, för mycket fett!";
    }
    else{
        self.nyttighetText.text = @"Kommer inte att göra dig fet!";
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
