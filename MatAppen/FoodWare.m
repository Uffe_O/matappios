//
//  FoodWare.m
//  MatAppen
//
//  Created by ITHS on 2016-02-23.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "FoodWare.h"

@implementation FoodWare

-(instancetype) initWithText: (NSString *)text andEnergi: (NSString*)energi
                  andProtein:(NSString*)protein
                     andFett:(NSString*)fett
                andVitaminer:(NSString*)vitaminer{
    self = [super init];
    if(self) {
        self.text = text;
        self.energi = energi;
        self.protein = protein;
        self.fett = fett;
        self.vitaminer = vitaminer;
    }
    return self;
}

@end
